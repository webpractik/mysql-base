SHELL := /bin/bash

help:
	@grep -E '^[1-9a-zA-Z_-]+:.*?## .*$$|(^#--)' $(MAKEFILE_LIST) \
	| awk 'BEGIN {FS = ":.*?## "}; {printf "\033[32m %-43s\033[0m %s\n", $$1, $$2}' \
	| sed -e 's/\[32m #-- /[33m/'

#-- First Run
install: ## первый запуск проекта
	@wpdeploy local-services down
	@dl service up
	@dl env
	@dl up
	@echo wait a bit...
	@echo
	@sleep 15
	@docker cp cleardb.sql mysql-base_db:/
	@docker exec -i mysql-base_db mysql -uroot -proot db < cleardb.sql
	dl up

#-- Project
up: ## запуск проекта
	@wpdeploy local-services down
	@dl service up
	dl up

down: ## остановка проекта
	dl down
