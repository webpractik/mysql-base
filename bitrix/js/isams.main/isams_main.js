/**
 * isams.send main scripts
 */

jQuery(document).ready(function () {

    // false для checkbox
    $(':checkbox').on('change', function () {
        if (this.checked) {
            $(this).val('1');
        } else {
            $(this).val('0');
        }
    });

    // Ручное отправление заявок в iSAMS
    $('#request_user').click(function (event) {
        event.preventDefault();
        var btn = this;

        $(btn).fadeTo(0, 0.5);
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: '/api/job/reloadqueue/',
            data: {signed: $('#signed').val()}
        }).done(function (data) {
            $('.isams-response').html(data['message']).fadeIn(500);

        }).fail(function (data) {
            console.log(data);
            alert('Error. Please, refresh page!');
        }).always(function () {
            $(btn).fadeTo(0, 1);
        });
    });

    // Значение поля School Year
    $('#request_year').click(function (event) {
        event.preventDefault();
        var btn = this;

        $(btn).fadeTo(0, 0.5);
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: '/api/job/set-school-year/',
            data: {year: $('#year').val()}
        }).done(function (data) {
            $('.isams-response').html(data['message']).fadeIn(500);

        }).fail(function (data) {
            console.log(data);
            alert('Error. Please, refresh page!');
        }).always(function () {
            $(btn).fadeTo(0, 1);
        });
    });

    // Обновление всех статусов
    $('#get-all-status').click(function (event) {
        event.preventDefault();
        var btn = this;

        $(btn).fadeTo(0, 0.5);
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: '/api/job/get-isams-status/',
            data: {getAllStatus: true}
        }).done(function (data) {
            $('.isams-response').html(data['message']).fadeIn(500);

        }).fail(function (data) {
            console.log(data);
            alert('Error. Please, refresh page!');
        }).always(function () {
            $(btn).fadeTo(0, 1);
        });
    });

    // Обновление одного статуса
    $('#get-app-status').click(function (event) {
        event.preventDefault();
        var btn = this;

        $(btn).fadeTo(0, 0.5);
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: '/api/job/get-isams-status/',
            data: {getAppStatus: true, crmid: $('#crmid').val()}
        }).done(function (data) {
            $('.isams-response').html(data['message']).fadeIn(500);

        }).fail(function (data) {
            console.log(data);
            alert('Error. Please, refresh page!');
        }).always(function () {
            $(btn).fadeTo(0, 1);
        });
    });

});
