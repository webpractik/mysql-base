/**
 * mindbox main scripts
 */

jQuery(document).ready(function () {

    // false для checkbox
    $(':checkbox').on('change', function () {
        if (this.checked) {
            $(this).val('1');
        } else {
            $(this).val('0');
        }
    });

    // Ручное отправление заявок в mindbox
    $('#request_user').click(function (event) {
        event.preventDefault();
        var btn = this;

        $(btn).fadeTo(0, 0.5);
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: '/api/job/reload-mindbox-queue/',
            data: {signed: $('#signed').val()}
        }).done(function (data) {
            $('.mindbox-response').html(data['message']).fadeIn(500);

        }).fail(function (data) {
            console.log(data);
            alert('Error. Please, refresh page!');
        }).always(function () {
            $(btn).fadeTo(0, 1);
        });
    });
});
